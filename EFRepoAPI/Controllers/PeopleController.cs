﻿using Microsoft.AspNetCore.Mvc;

namespace EFRepoAPI.Controllers
{
    using System;
    using System.Net;
    using System.Threading.Tasks;

    using EFRepoAPI.Dtos;

    using EFRepoDomain;

    using Microsoft.Extensions.Logging;

    using Repository.Interface;

    [Route("api/[controller]")]
    [ApiController]
    public class PeopleController : ControllerBase
    {
        private readonly ILogger<PeopleController> logger;

        private readonly IUnitOfWork unitOfWork;

        public PeopleController(ILogger<PeopleController> logger, IUnitOfWork uow)
        {
            this.unitOfWork = uow;
            this.logger = logger;
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<PersonDto> Get(Guid id)
        {
            var person = await this.unitOfWork.GetReadOnlyRepository<Person>().FirstOrDefaultAsync(p => p.Id == id);
            if (person != null)
            {
                return new PersonDto
                           {
                               FirstName = person.FirstName,
                               LastName = person.LastName,
                               Height = person.Height,
                               DateOfBirth = person.DateOfBirth
                           };
            }

            return null;
        }

        [HttpPost]
        public async Task<IActionResult> Create(PersonDto person)
        {
            var newPerson = new Person
                        {
                            FirstName = person.FirstName,
                            LastName = person.LastName,
                            Height = person.Height,
                            DateOfBirth = person.DateOfBirth
                        };
            
            this.unitOfWork.GetRepository<Person>().Add(newPerson);
            var result = await this.unitOfWork.CommitAsync();

            if (result == 1)
            {
                return StatusCode((int)HttpStatusCode.Created);
            }

            return this.BadRequest();
        }
    }
}
