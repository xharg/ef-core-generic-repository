﻿namespace EFRepoAPI.Dtos
{
    using System;

    public class PersonDto
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public double? Height { get; set; }

        public DateTime? DateOfBirth { get; set; }
    }
}
