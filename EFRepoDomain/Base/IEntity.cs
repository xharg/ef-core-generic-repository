﻿using System;

namespace EFRepoDomain.Base
{
    public interface IEntity
    {
        Guid Id { get; set; }
    }
}
