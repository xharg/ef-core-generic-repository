﻿using System;

namespace EFRepoDomain
{
    using EFRepoDomain.Base;

    public class Person : IEntity
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public double? Height { get; set; }

        public DateTime? DateOfBirth { get; set; }
    }
}
