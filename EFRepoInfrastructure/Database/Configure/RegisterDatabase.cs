﻿namespace EFRepoInfrastructure.Database.Configure
{
    using EFRepoInfrastructure.Database;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    using Repository.Configure;

    public static class RegisterDatabase
    {
        public static IServiceCollection AddDatabase(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<MainContext>(options =>
                options.UseNpgsql(configuration.GetConnectionString("DefaultConnection")));

            services.AddUnitOfWork<MainContext>();
            return services;
        }
    }
}
