﻿namespace EFRepoInfrastructure.Database
{
    using EFRepoDomain;

    using Microsoft.EntityFrameworkCore;

    public class MainContext : DbContext
    {
        public MainContext(DbContextOptions<MainContext> options)
            : base(options)
        {
        }

        public DbSet<Person> People { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Person>(entity =>
                {
                    entity.ToTable("people").HasKey(t => t.Id);

                    entity.Property(e => e.Id).HasColumnName("id").IsRequired().HasDefaultValueSql("uuid_generate_v1()");

                    entity.Property(e => e.FirstName)
                        .HasColumnName("FirstName")
                        .HasColumnType("varchar(50)")
                        .IsRequired();

                    entity.Property(e => e.LastName)
                        .HasColumnName("LastName")
                        .HasColumnType("varchar(100)");

                    entity.Property(e => e.Height).HasColumnName("Height").HasColumnType("DOUBLE PRECISION");

                    entity.Property(e => e.DateOfBirth).HasColumnName("Dob").HasColumnType("timestamp");

                });
        }
    }
}

