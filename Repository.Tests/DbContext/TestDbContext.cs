namespace Repository.Tests.DbContext
{
    using Microsoft.EntityFrameworkCore;

    using Repository.Tests.TestEntities;

    public class TestDbContext: DbContext
    {
        public TestDbContext(DbContextOptions<TestDbContext> options) : base(options)
        {
        }

        public virtual DbSet<TestEntity> TestEntities { get; set; }
        public virtual DbSet<TestSubEntity> TestSubEntities{ get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TestEntity>(entity =>
                {
                    entity.ToTable("TestEntityTable");
                    entity.Property(e => e.Id).HasColumnName("id");

                    entity.Property(e => e.EntityName)
                        .HasColumnName("Name")
                        .HasColumnType("varchar(100)");
                });

            modelBuilder.Entity<TestSubEntity>(entity =>
                {
                    entity.ToTable("TestSubEntityTable");

                    entity.HasIndex(e => e.ParentId).HasName("testSubEntity_testEntityId");
                    entity.Property(e => e.ParentId).HasColumnName("ParentId");

                    entity.Property(e => e.Name)
                        .HasColumnName("Name")
                        .HasColumnType("varchar(50)");

                    entity.HasOne(d => d.Parent)
                        .WithMany(p => p.Children)
                        .HasForeignKey(d => d.ParentId)
                        .HasConstraintName("testSubEntity_testEntityId");
                });
        }
    }
}