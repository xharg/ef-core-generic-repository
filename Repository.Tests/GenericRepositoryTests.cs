namespace Repository.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.EntityFrameworkCore;

    using Repository.Tests.TestEntities;
    using Repository.Tests.TestFixtures;

    using Xunit;

    public class GenericRepositoryTests : IClassFixture<SqlLiteTestFixture>
    {
        private readonly SqlLiteTestFixture fixture;

        public GenericRepositoryTests(SqlLiteTestFixture fixture)
        {
            this.fixture = fixture;
            this.fixture.Seed();
        }

        #region Command tests

        [Fact]
        public void AddSavesSingleEntity()
        {
            var id = Guid.Parse("55531808-4b6c-4174-a643-c70a12102661");

            var testObject = new TestEntity()
            {
                Id = id,
                EntityName = "Test1",
                SomeDate = DateTime.Now,
                Children = null
            };

            var repo = new GenericRepository<TestEntity>(this.fixture.Context);
            repo.Add(testObject);
            this.fixture.Context.SaveChanges();

            var savedObject = this.fixture.Context.TestEntities.FirstOrDefault(t => t.Id == id);
            Assert.NotNull(savedObject);
        }

        [Fact]
        public void AddSavesSingleEntityWithCorrectData()
        {
            var id = Guid.Parse("44431808-4b6c-4174-a643-c70a12102661");
            var name = "TestName1";
            var date = DateTime.Now;

            var testObject = new TestEntity()
            {
                Id = id,
                EntityName = name,
                SomeDate = date,
                Children = null
            };

            var repo = new GenericRepository<TestEntity>(this.fixture.Context);
            repo.Add(testObject);
            this.fixture.Context.SaveChanges();

            var savedObject = this.fixture.Context.TestEntities.FirstOrDefault(t => t.Id == id);
            Assert.NotNull(savedObject);
            Assert.Equal(name, savedObject.EntityName);
            Assert.Equal(date, savedObject.SomeDate);
        }

        [Fact]
        public void AddSavesSingleEntityWithNestedCollection()
        {
            var id = Guid.Parse("33331808-4b6c-4174-a643-c70a12102661");
            var nestedId = Guid.Parse("ef831808-4b6c-4174-a643-c70a12102005");

            var testObject = new TestEntity()
            {
                Id = id,
                EntityName = "TestName",
                SomeDate = DateTime.Now,
                Children = new List<TestSubEntity>
                                               {
                                                   new TestSubEntity { Id = nestedId, Age=3, Name="abc3" }
                                               }
            };

            var repo = new GenericRepository<TestEntity>(this.fixture.Context);
            repo.Add(testObject);
            this.fixture.Context.SaveChanges();

            var savedObject = this.fixture.Context.TestEntities.Include(x => x.Children).FirstOrDefault(t => t.Id == id);
            Assert.NotNull(savedObject);
            Assert.Equal(1, savedObject.Children.Count);
        }

        [Fact]
        public void AddSavesNumberOfEntites()
        {
            var id1 = Guid.Parse("10031808-4b6c-4174-a643-c70a12102661");
            var id2 = Guid.Parse("10131808-4b6c-4174-a643-c70a12102661");

            var testObject1 = new TestEntity()
            {
                Id = id1,
                EntityName = "Test1",
                SomeDate = DateTime.Now,
                Children = null
            };

            var testObject2 = new TestEntity()
            {
                Id = id2,
                EntityName = "Test2",
                SomeDate = DateTime.Now,
                Children = null
            };

            var repo = new GenericRepository<TestEntity>(this.fixture.Context);
            repo.Add(testObject1, testObject2);
            this.fixture.Context.SaveChanges();

            var savedObject1 = this.fixture.Context.TestEntities.FirstOrDefault(t => t.Id == id1);
            var savedObject2 = this.fixture.Context.TestEntities.FirstOrDefault(t => t.Id == id2);
            Assert.NotNull(savedObject1);
            Assert.NotNull(savedObject2);
        }

        [Fact]
        public void AddSavesCollectionOfEntites()
        {
            var id1 = Guid.NewGuid();
            var id2 = Guid.NewGuid();

            var testObject1 = new TestEntity()
                                  {
                                      Id = id1,
                                      EntityName = "Test1",
                                      SomeDate = DateTime.Now,
                                      Children = null
                                  };

            var testObject2 = new TestEntity()
                                  {
                                      Id = id2,
                                      EntityName = "Test2",
                                      SomeDate = DateTime.Now,
                                      Children = null
                                  };

            var collection = new List<TestEntity> { testObject2, testObject1 };

            var repo = new GenericRepository<TestEntity>(this.fixture.Context);
            repo.Add(collection);
            this.fixture.Context.SaveChanges();

            var savedObject1 = this.fixture.Context.TestEntities.FirstOrDefault(t => t.Id == id1);
            var savedObject2 = this.fixture.Context.TestEntities.FirstOrDefault(t => t.Id == id2);
            Assert.NotNull(savedObject1);
            Assert.NotNull(savedObject2);
        }

        [Fact]
        public void UpdateModifiesEntity()
        {
            var newName = "Modified" + Guid.NewGuid();
            var newDate = DateTime.Now;
            var entity = this.fixture.Context.TestEntities.First();
            var id = entity.Id;

            entity.EntityName = newName;
            entity.SomeDate = newDate;

            var repo = new GenericRepository<TestEntity>(this.fixture.Context);
            repo.Update(entity);
            this.fixture.Context.SaveChanges();

            var savedObject = this.fixture.Context.TestEntities.FirstOrDefault(t => t.Id == id);
            Assert.NotNull(savedObject);
            Assert.Equal(newName, savedObject.EntityName);
            Assert.Equal(newDate, savedObject.SomeDate);
        }

        [Fact]
        public void UpdateModifiesEntities()
        {
            var newName = "Modified" + Guid.NewGuid();
            var newDate = DateTime.Now;
            var entities = this.fixture.Context.TestEntities;

            foreach (var ent in entities)
            {
                ent.EntityName = newName;
                ent.SomeDate = newDate;
            }
            
            var repo = new GenericRepository<TestEntity>(this.fixture.Context);
            repo.Update(entities);
            this.fixture.Context.SaveChanges();

            var firstObject = this.fixture.Context.TestEntities.First();
            var lastObject = this.fixture.Context.TestEntities.First();

            Assert.Equal(newName, firstObject.EntityName);
            Assert.Equal(newDate, firstObject.SomeDate);
            Assert.Equal(newName, lastObject.EntityName);
            Assert.Equal(newDate, lastObject.SomeDate);
        }


        [Fact]
        public void UpdateModifiesEntityGraph()
        {
            var newName = "Modified" + Guid.NewGuid();
            var newAge = 333;
            var entity = this.fixture.Context.TestEntities.Include(x => x.Children).First();
            var id = entity.Id;
            var firstNested = entity.Children.First();

            firstNested.Name = newName;
            firstNested.Age = newAge;

            var repo = new GenericRepository<TestEntity>(this.fixture.Context);
            repo.Update(entity);
            this.fixture.Context.SaveChanges();

            var savedObject = this.fixture.Context.TestEntities.FirstOrDefault(t => t.Id == id);
            var savedFirstNested = savedObject.Children.First();
            Assert.NotNull(savedFirstNested);
            Assert.Equal(newName, savedFirstNested.Name);
            Assert.Equal(newAge, savedFirstNested.Age);
        }

        [Fact]
        public void DeleteRemovesEntity()
        {
            var toDelete = this.fixture.Context.TestEntities.First();
            var id = toDelete.Id;

            var repo = new GenericRepository<TestEntity>(this.fixture.Context);
            repo.Delete(toDelete);
            this.fixture.Context.SaveChanges();

            var deleted = this.fixture.Context.TestEntities.FirstOrDefault(x => x.Id == id);
            Assert.Null(deleted);
        }

        [Fact]
        public void DeleteRemovesEntityWithNested()
        {
            var toDelete = this.fixture.Context.TestEntities.First();
            var id = toDelete.Id;
            var initialNestedCount = this.fixture.Context.TestSubEntities.Count();
            var toDeleteNestedCount = toDelete.Children.Count;

            var repo = new GenericRepository<TestEntity>(this.fixture.Context);
            repo.Delete(toDelete);
            this.fixture.Context.SaveChanges();

            var afterDeleteNestedCount = this.fixture.Context.TestSubEntities.Count();
            Assert.Equal(initialNestedCount - toDeleteNestedCount, afterDeleteNestedCount);
        }

        #endregion

        #region Query tests

        [Fact]
        public void FindReturnsCorrectEntity()
        {
            Guid id = Guid.Parse("ef831808-4b6c-4174-a643-c70a12102661");
            var repo = new GenericRepository<TestEntity>(this.fixture.Context);

            var result = repo.Find(id);

            Assert.Equal(id, result.Id);
        }

        [Fact]
        public void GetListReturnsAllEntities()
        {
            var repo = new GenericRepository<TestEntity>(this.fixture.Context);

            var result = repo.GetList();

            Assert.NotNull(result);
            Assert.Equal(this.fixture.Context.TestEntities.Count(), result.Count());
        }

        [Fact]
        public void GetListReturnsFilteredEntities()
        {
            Guid id = Guid.Parse("ef831808-4b6c-4174-a643-c70a12102661");
            var repo = new GenericRepository<TestEntity>(this.fixture.Context);

            var result = repo.GetList(entity => entity.Id.Equals(id));

            Assert.NotNull(result);
            Assert.Equal(1, result.Count());
        }

        [Fact]
        public void GetListReturnsIncludesChildren()
        {
            var repo = new GenericRepository<TestEntity>(this.fixture.Context);

            var result = repo.GetList(include: query => query.Include(x => x.Children));
            var first = result.FirstOrDefault();

            Assert.Equal(this.fixture.Context.TestEntities.Count(), result.Count());
            Assert.NotNull(first);
            Assert.StartsWith("abc", first.Children.FirstOrDefault().Name);
        }

        [Fact]
        public void GetListReturnsOrderedEntities()
        {
            var repo = new GenericRepository<TestEntity>(this.fixture.Context);

            var result = repo.GetList(orderBy: query => query.OrderByDescending(x => x.EntityName));
            var first = result.FirstOrDefault();

            Assert.NotNull(first);
            Assert.Equal("Test2", first.EntityName);
        }

        [Fact]
        public void GetSubsetListReturnsCorrectValuesCount()
        {
            var repo = new GenericRepository<TestEntity>(this.fixture.Context);

            var result = repo.GetSubsetList<string>(selector: x => x.EntityName);

            Assert.NotNull(result);
            Assert.Equal(2, result.Count());
        }

        [Fact]
        public void GetSubsetListReturnsCorrectValues()
        {
            var repo = new GenericRepository<TestEntity>(this.fixture.Context);

            var result = repo.GetSubsetList<string>(x => x.EntityName, orderBy: query => query.OrderBy(x => x.EntityName));
            var first = result.FirstOrDefault();

            Assert.NotNull(first);
            Assert.Equal("Test1", first);
        }


        [Theory]
        [InlineData("ef831808-4b6c-4174-a643-c70a12102661")]
        [InlineData("ef831808-4b6c-4174-a643-c70a12102662")]
        public void FirstOrDefaultReturnsCorrectEntity(string id)
        {
            var repo = new GenericRepository<TestEntity>(this.fixture.Context);

            var result = repo.FirstOrDefault(e => e.Id.Equals(Guid.Parse(id)));

            Assert.NotNull(result);
        }

        [Fact]
        public void FirstOrDefaultReturnsFirstOrdered()
        {
            var repo = new GenericRepository<TestEntity>(this.fixture.Context);
            var result = repo.FirstOrDefault(orderBy: query => query.OrderByDescending(x => x.EntityName));

            Assert.Equal("Test2", result.EntityName);
        }

        [Fact]
        public void FirstOrDefaultDoesNotIncludeChildrenWhenNotSpecified()
        {
            var repo = new GenericRepository<TestEntity>(this.fixture.Context);
            var result = repo.FirstOrDefault();

            Assert.NotNull(result);
            Assert.Null(result.Children);
        }

        [Fact]
        public void FirstOrDefaultIncludesChildren()
        {
            var repo = new GenericRepository<TestEntity>(this.fixture.Context);
            var result = repo.FirstOrDefault(include: query => query.Include(x => x.Children));

            Assert.NotNull(result);
            Assert.NotNull(result.Children);
            Assert.StartsWith("abc", result.Children.FirstOrDefault().Name);
        }

        [Fact]
        public void FirstOrDefaultIncludedChildrenHasData()
        {
            var repo = new GenericRepository<TestEntity>(this.fixture.Context);
            var result = repo.FirstOrDefault(include: query => query.Include(x => x.Children));

            Assert.NotNull(result.Children);
            Assert.StartsWith("abc", result.Children.FirstOrDefault().Name);
        }

        #endregion
    }
}
