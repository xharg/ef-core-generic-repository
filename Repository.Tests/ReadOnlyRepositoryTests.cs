namespace Repository.Tests
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    using Microsoft.EntityFrameworkCore;

    using Repository.Tests.TestEntities;
    using Repository.Tests.TestFixtures;

    using Xunit;

    public class ReadOnlyRepositoryTests : IClassFixture<SqlLiteTestFixture>
    {
        private readonly SqlLiteTestFixture fixture;

        public ReadOnlyRepositoryTests(SqlLiteTestFixture fixture)
        {
            this.fixture = fixture;
            this.fixture.Seed();
        }

        [Fact]
        public void FindReturnsCorrectEntity()
        {

            Guid id = Guid.Parse("ef831808-4b6c-4174-a643-c70a12102661");
            var repo = new ReadOnlyRepository<TestEntity>(this.fixture.Context);

            var result = repo.Find(id);

            Assert.Equal(id, result.Id);
        }

        [Fact]
        public async Task FindAsyncReturnsCorrectEntity()
        {

            Guid id = Guid.Parse("ef831808-4b6c-4174-a643-c70a12102661");
            var repo = new ReadOnlyRepository<TestEntity>(this.fixture.Context);

            var result = await repo.FindAsync(id);

            Assert.Equal(id, result.Id);
        }

        [Fact]
        public void GetListReturnsAllEntities()
        {
            var repo = new ReadOnlyRepository<TestEntity>(this.fixture.Context);

            var result = repo.GetList();

            Assert.NotNull(result);
            Assert.Equal(this.fixture.Context.TestEntities.Count(), result.Count());
        }

        [Fact]
        public void GetListReturnsFilteredEntities()
        {
            Guid id = Guid.Parse("ef831808-4b6c-4174-a643-c70a12102661");
            var repo = new ReadOnlyRepository<TestEntity>(this.fixture.Context);

            var result = repo.GetList(entity => entity.Id.Equals(id));

            Assert.NotNull(result);
            Assert.Equal(1, result.Count());
        }

        [Fact]
        public void GetListReturnsIncludesChildren()
        {
            var repo = new ReadOnlyRepository<TestEntity>(this.fixture.Context);

            var result = repo.GetList(include: query => query.Include(x => x.Children));
            var first = result.FirstOrDefault();

            Assert.Equal(this.fixture.Context.TestEntities.Count(), result.Count());
            Assert.NotNull(first);
            Assert.StartsWith("abc", first.Children.FirstOrDefault().Name);
        }

        [Fact]
        public void GetListReturnsOrderedEntities()
        {
            var repo = new ReadOnlyRepository<TestEntity>(this.fixture.Context);

            var result = repo.GetList(orderBy: query => query.OrderByDescending(x => x.EntityName));
            var first = result.FirstOrDefault();

            Assert.NotNull(first);
            Assert.Equal("Test2", first.EntityName);
        }

        [Fact]
        public void GetSubsetListReturnsCorrectValuesCount()
        {
            var repo = new ReadOnlyRepository<TestEntity>(this.fixture.Context);

            var result = repo.GetSubsetList<string>(selector: x => x.EntityName);

            Assert.NotNull(result);
            Assert.Equal(2, result.Count());
        }

        [Fact]
        public void GetSubsetListReturnsCorrectValues()
        {
            var repo = new ReadOnlyRepository<TestEntity>(this.fixture.Context);

            var result = repo.GetSubsetList<string>(x => x.EntityName, orderBy: query => query.OrderBy(x => x.EntityName));
            var first = result.FirstOrDefault();

            Assert.NotNull(first);
            Assert.Equal("Test1", first);
        }


        [Theory]
        [InlineData("ef831808-4b6c-4174-a643-c70a12102661")]
        [InlineData("ef831808-4b6c-4174-a643-c70a12102662")]
        public void FirstOrDefaultReturnsCorrectEntity(string id)
        {
            var repo = new ReadOnlyRepository<TestEntity>(this.fixture.Context);

            var result = repo.FirstOrDefault(e => e.Id.Equals(Guid.Parse(id)));

            Assert.NotNull(result);
        }

        [Fact]
        public void FirstOrDefaultReturnsFirstOrdered()
        {
            var repo = new ReadOnlyRepository<TestEntity>(this.fixture.Context);
            var result = repo.FirstOrDefault(orderBy: query => query.OrderByDescending(x => x.EntityName));

            Assert.Equal("Test2", result.EntityName);
        }

        [Fact]
        public void FirstOrDefaultDoesNotIncludeChildrenWhenNotSpecified()
        {
            var repo = new ReadOnlyRepository<TestEntity>(this.fixture.Context);
            var result = repo.FirstOrDefault();

            Assert.NotNull(result);
            Assert.Null(result.Children);
        }

        [Fact]
        public void FirstOrDefaultIncludesChildren()
        {
            var repo = new ReadOnlyRepository<TestEntity>(this.fixture.Context);
            var result = repo.FirstOrDefault(include: query => query.Include(x => x.Children));

            Assert.NotNull(result);
            Assert.NotNull(result.Children);
            Assert.StartsWith("abc", result.Children.FirstOrDefault().Name);
        }

        [Fact]
        public void FirstOrDefaultIncludedChildrenHasData()
        {
            var repo = new ReadOnlyRepository<TestEntity>(this.fixture.Context);
            var result = repo.FirstOrDefault(include: query => query.Include(x => x.Children));

            Assert.NotNull(result.Children);
            Assert.StartsWith("abc", result.Children.FirstOrDefault().Name);
        }

        [Theory]
        [InlineData("ef831808-4b6c-4174-a643-c70a12102661")]
        [InlineData("ef831808-4b6c-4174-a643-c70a12102662")]
        public async Task FirstOrDefaultAsyncReturnsCorrectEntity(string id)
        {
            var repo = new ReadOnlyRepository<TestEntity>(this.fixture.Context);

            var result = await repo.FirstOrDefaultAsync(e => e.Id.Equals(Guid.Parse(id)));

            Assert.NotNull(result);
        }

        [Fact]
        public async Task FirstOrDefaultAsyncReturnsFirstOrdered()
        {
            var repo = new ReadOnlyRepository<TestEntity>(this.fixture.Context);
            var result = await repo.FirstOrDefaultAsync(orderBy: query => query.OrderByDescending(x => x.EntityName));

            Assert.Equal("Test2", result.EntityName);
        }

        [Fact]
        public async Task FirstOrDefaultAsyncDoesNotIncludeChildrenWhenNotSpecified()
        {
            var repo = new ReadOnlyRepository<TestEntity>(this.fixture.Context);
            var result = await repo.FirstOrDefaultAsync();

            Assert.NotNull(result);
            Assert.Null(result.Children);
        }

        [Fact]
        public async Task FirstOrDefaultAsyncIncludesChildren()
        {
            var repo = new ReadOnlyRepository<TestEntity>(this.fixture.Context);
            var result = await repo.FirstOrDefaultAsync(include: query => query.Include(x => x.Children));

            Assert.NotNull(result);
            Assert.NotNull(result.Children);
            Assert.StartsWith("abc", result.Children.FirstOrDefault().Name);
        }

        [Fact]
        public async Task FirstOrDefaultAsyncIncludedChildrenHasData()
        {
            var repo = new ReadOnlyRepository<TestEntity>(this.fixture.Context);
            var result = await repo.FirstOrDefaultAsync(include: query => query.Include(x => x.Children));

            Assert.NotNull(result.Children);
            Assert.StartsWith("abc", result.Children.FirstOrDefault().Name);
        }

        [Theory]
        [InlineData("Test1")]
        [InlineData("Test2")]
        public void QueryReturnsCorrectEntity(string name)
        {
            var repo = new ReadOnlyRepository<TestEntity>(this.fixture.Context);
            var sql = "SELECT * FROM TestEntityTable WHERE Name={0}";
            
            var entity = repo.Query(sql, name).FirstOrDefault();

            Assert.NotNull(entity);
            Assert.Equal(entity.EntityName, name);
        }
    }
}
