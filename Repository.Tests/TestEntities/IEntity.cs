﻿using System;

namespace Repository.Tests.TestEntities
{
    public interface IEntity
    {
        Guid Id { get; set; }
    }
}
