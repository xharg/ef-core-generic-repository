namespace Repository.Tests.TestEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class TestEntity : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [MaxLength(100)]
        public string EntityName { get; set; }

        public DateTime SomeDate { get; set; }

        public ICollection<TestSubEntity> Children { get; set; }
    }
}