namespace Repository.Tests.TestEntities
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class TestSubEntity :IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public string Name { get; set; }

        public int Age { get; set; }

        public Guid ParentId {get; set;}

        public TestEntity Parent {get; set;}
    }
}