namespace Repository.Tests.TestFixtures
{
    using System;
    using System.Collections.Generic;
    using Microsoft.EntityFrameworkCore;

    using Repository.Tests.DbContext;
    using Repository.Tests.TestEntities;

    public class InMemoryTestFixture : IDisposable
    {
        private TestDbContext context;

        public TestDbContext Context => this.context ?? (this.context = InMemoryContext());

        public InMemoryTestFixture()
        {
        }

        public void Dispose()
        {
            this.context?.Dispose();
        }

        public void Seed()
        {
            this.Context.TestEntities.RemoveRange(this.Context.TestEntities);
            this.Context.TestEntities.Add(new TestEntity()
            {
                Id = Guid.Parse("ef831808-4b6c-4174-a643-c70a12102661"),
                EntityName = "Test1",
                SomeDate = DateTime.Now,
                Children = new List<TestSubEntity>
                {
                    new TestSubEntity { Id = Guid.Parse("ef831808-4b6c-4174-a643-c70a12102001"), Age=1, Name="abc1" },
                    new TestSubEntity { Id = Guid.Parse("ef831808-4b6c-4174-a643-c70a12102002"), Age=2, Name="abc2" }
                }
            });

            this.Context.TestEntities.Add(new TestEntity()
            {
                Id = Guid.Parse("ef831808-4b6c-4174-a643-c70a12102662"),
                EntityName = "Test2",
                SomeDate = DateTime.Now.AddMinutes(1),
                Children = new List<TestSubEntity>
                {
                    new TestSubEntity { Id = Guid.Parse("ef831808-4b6c-4174-a643-c70a12102003"), Age=3, Name="abc3" },
                    new TestSubEntity { Id = Guid.Parse("ef831808-4b6c-4174-a643-c70a12102004"), Age=4, Name="abc4" }
                }
            });
            this.Context.SaveChanges();
        }

        public void RemoveAll()
        {
            this.Context.TestEntities.RemoveRange(this.Context.TestEntities);
            this.Context.SaveChanges();
        }

        private static TestDbContext InMemoryContext()
        {
            var options = new DbContextOptionsBuilder<TestDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .EnableSensitiveDataLogging()
                .Options;
            var context = new TestDbContext(options);

            return context;
        }
    }
}