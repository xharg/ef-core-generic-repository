﻿namespace Repository.Tests
{
    using System;

    using Repository.Interface;
    using Repository.Tests.DbContext;
    using Repository.Tests.TestEntities;
    using Repository.Tests.TestFixtures;

    using Xunit;

    public class UnitOfWorkTests : IClassFixture<SqlLiteTestFixture>
    {
        private readonly SqlLiteTestFixture fixture;

        public UnitOfWorkTests(SqlLiteTestFixture fixture)
        {
            this.fixture = fixture;
        }

        [Fact]
        public void GetRepositoryReturnsCorrectObject()
        {
            var uow = new UnitOfWork<TestDbContext>(this.fixture.Context);

            var repository = uow.GetRepository<TestEntity>();

            Assert.NotNull(repository);
            Assert.IsAssignableFrom<IGenericRepository<TestEntity>>(repository);
        }

        [Fact]
        public void GetRepositoryAlwaysReturnsSameObjectPerRepositoryType()
        {
            var uow = new UnitOfWork<TestDbContext>(this.fixture.Context);

            var repository = uow.GetRepository<TestEntity>();
            var repository2 = uow.GetRepository<TestEntity>();

            Assert.NotNull(repository);
            Assert.NotNull(repository2);
            Assert.Equal(repository, repository2);
        }

        [Fact]
        public void GetReadOnlyRepositoryAlwaysReturnsSameObjectPerRepositoryType()
        {
            var uow = new UnitOfWork<TestDbContext>(this.fixture.Context);

            var repository = uow.GetReadOnlyRepository<TestEntity>();
            var repository2 = uow.GetReadOnlyRepository<TestEntity>();

            Assert.NotNull(repository);
            Assert.NotNull(repository2);
            Assert.Equal(repository, repository2);
        }

        [Fact]
        public void GetReadOnlyRepositoryAndGetRepositoryRetunDifferentObjects()
        {
            var uow = new UnitOfWork<TestDbContext>(this.fixture.Context);

            var repository = uow.GetReadOnlyRepository<TestEntity>();
            var repository2 = uow.GetRepository<TestEntity>();

            Assert.NotNull(repository);
            Assert.NotNull(repository2);
            Assert.NotEqual(repository, repository2);
        }


        [Theory]
        [InlineData("ef831808-4b6c-4174-a643-c70a12102661")]
        [InlineData("ef831808-4b6c-4174-a643-c70a12102662")]
        public void UowRepositoryReturnsCorrectEntity(string id)
        {
            this.fixture.Seed();
            var uow = new UnitOfWork<TestDbContext>(this.fixture.Context);

            var repository = uow.GetReadOnlyRepository<TestEntity>();

            var testEntity = repository.FirstOrDefault(x => x.Id == Guid.Parse(id));

            Assert.NotNull(testEntity);
            Assert.Equal(Guid.Parse(id), testEntity.Id);
        }
    }
}
