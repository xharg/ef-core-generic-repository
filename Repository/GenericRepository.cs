namespace Repository
{
    using System.Collections.Generic;

    using Microsoft.EntityFrameworkCore;

    using Repository.Interface;

    public class GenericRepository<T> : BaseRepository<T>, IGenericRepository<T> where T : class
    {
        public GenericRepository(DbContext context) : base(context)
        {
        }

        public void Add(T entity)
        {
            this._dbSet.Add(entity);
        }

        public void Add(params T[] entities)
        {
            _dbSet.AddRange(entities);
        }

        public void Add(IEnumerable<T> entities)
        {
            _dbSet.AddRange(entities);
        }

        public void Update(T entity)
        {
            _dbSet.Update(entity);
        }

        public void Update(params T[] entities)
        {
            _dbSet.UpdateRange(entities);
        }

        public void Update(IEnumerable<T> entities)
        {
            _dbSet.UpdateRange(entities);
        }

        public void Delete(params T[] entities)
        {
            _dbSet.RemoveRange(entities);
        }

        public void Delete(IEnumerable<T> entities)
        {
            _dbSet.RemoveRange(entities);
        }
    }
}