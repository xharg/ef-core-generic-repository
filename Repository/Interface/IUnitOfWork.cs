﻿namespace Repository.Interface
{
    using System;
    using System.Threading.Tasks;

    using Microsoft.EntityFrameworkCore;

    public interface IUnitOfWork : IDisposable
    {
        IGenericRepository<TEntity> GetRepository<TEntity>() where TEntity : class;
        IReadRepository<TEntity> GetReadOnlyRepository<TEntity>() where TEntity : class;
        
        int Commit();
        Task<int> CommitAsync();
    }

    public interface IUnitOfWork<TContext> : IUnitOfWork where TContext : DbContext
    {
        TContext Context { get; }
    }
}
