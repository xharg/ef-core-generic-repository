﻿namespace Repository
{
    using Microsoft.EntityFrameworkCore;

    public class ReadOnlyRepository<T> : BaseRepository<T> where T: class
    {
        public ReadOnlyRepository(DbContext context):base(context)
        {
        }

    }
}
