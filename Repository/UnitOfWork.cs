﻿namespace Repository
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using Microsoft.EntityFrameworkCore;

    using Repository.Interface;

    public class UnitOfWork<TContext> : IUnitOfWork<TContext> where TContext : DbContext, IDisposable
    {
        private Dictionary<(Type type, string name), object> repositories;

        public UnitOfWork(TContext context)
        {
            this.Context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public IGenericRepository<TEntity> GetRepository<TEntity>() where TEntity : class
        {
            return (IGenericRepository<TEntity>)this.GetOrAddRepository(typeof(TEntity), new GenericRepository<TEntity>(this.Context));
        }
        
        public IReadRepository<TEntity> GetReadOnlyRepository<TEntity>() where TEntity : class
        {
            return (IReadRepository<TEntity>)this.GetOrAddRepository(typeof(TEntity),
                new ReadOnlyRepository<TEntity>(this.Context));
        }

        public TContext Context { get; }

        public int Commit()
        {
            return this.Context.SaveChanges();
        }

        public async Task<int> CommitAsync()
        {
            return await this.Context.SaveChangesAsync();
        }

        public void Dispose()
        {
            this.Context?.Dispose();
        }

        internal object GetOrAddRepository(Type type, object repo)
        {
            if (this.repositories == null)
            {
                this.repositories = new Dictionary<(Type type, string Name), object>();
            }

            if (this.repositories.TryGetValue((type, repo.GetType().FullName), out var repository))
            {
                return repository;
            }

            this.repositories.Add((type, repo.GetType().FullName), repo);
            return repo;
        }
    }
}
